var classrt_1_1_sphere =
[
    [ "Sphere", "d6/d6e/classrt_1_1_sphere.html#a30fecc25f909db8cae212d106f1865ec", null ],
    [ "get_center", "d6/d6e/classrt_1_1_sphere.html#a61bc033269504ded00239b83b08ffb12", null ],
    [ "get_color", "d6/d6e/classrt_1_1_sphere.html#a65fc6a1dffee2f4098ca393a9b57e07e", null ],
    [ "get_normal", "d6/d6e/classrt_1_1_sphere.html#ae9a5d71af6da7019a4af18e490f2de56", null ],
    [ "get_radius", "d6/d6e/classrt_1_1_sphere.html#a3e3ffcaad742a4fe317085fe51ba6466", null ],
    [ "intersect", "d6/d6e/classrt_1_1_sphere.html#ac7731bc9fcbddf60c198ac773d286628", null ],
    [ "send", "d6/d6e/classrt_1_1_sphere.html#a7a9b54b21dc054ba57e36c1db3433d41", null ],
    [ "center", "d6/d6e/classrt_1_1_sphere.html#aa6a6f8f233a5bd89830f51412f3b7e18", null ],
    [ "radius", "d6/d6e/classrt_1_1_sphere.html#a06706fa79276335b683619216dcb8005", null ]
];