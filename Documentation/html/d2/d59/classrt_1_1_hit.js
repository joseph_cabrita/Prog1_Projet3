var classrt_1_1_hit =
[
    [ "Hit", "d2/d59/classrt_1_1_hit.html#ac12fd708909210c4be23fd8e415110a6", null ],
    [ "~Hit", "d2/d59/classrt_1_1_hit.html#a6b06024fcf4bc21acd339461a549dc7c", null ],
    [ "get_color", "d2/d59/classrt_1_1_hit.html#aa474fa9ca8a05db6c1d742374a4a571d", null ],
    [ "get_normal", "d2/d59/classrt_1_1_hit.html#aa14a71db4407bbacb68a6d39f48ca4b2", null ],
    [ "get_point", "d2/d59/classrt_1_1_hit.html#acde1eed9d6f2f7bb5369ca6c6c23a71a", null ],
    [ "get_ray", "d2/d59/classrt_1_1_hit.html#aaaeccae4684ad7700bbfc3517d34843b", null ],
    [ "is_light", "d2/d59/classrt_1_1_hit.html#a0165da9fd6b78250b73a50753f8f915e", null ],
    [ "set_color", "d2/d59/classrt_1_1_hit.html#aa51a76902ed810ea85668ff3114ca46c", null ],
    [ "set_is_light", "d2/d59/classrt_1_1_hit.html#a10d39f827d17a101b4a6ccda3fb8dbc3", null ]
];