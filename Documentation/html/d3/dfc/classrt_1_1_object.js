var classrt_1_1_object =
[
    [ "get_color", "d3/dfc/classrt_1_1_object.html#ac871186186e29b72dd014f65582a1da6", null ],
    [ "get_diffuse", "d3/dfc/classrt_1_1_object.html#ac402c7c3d1d6414c8d58abb85345cd3d", null ],
    [ "get_normal", "d3/dfc/classrt_1_1_object.html#a4781d86db2ab215309c8c8a7e2c351de", null ],
    [ "get_specular", "d3/dfc/classrt_1_1_object.html#af53289fda3b7c53041611135304f5e64", null ],
    [ "get_specular_exponent", "d3/dfc/classrt_1_1_object.html#a20b353a9820102c831f9b0867e1c415b", null ],
    [ "get_type", "d3/dfc/classrt_1_1_object.html#af6828825ee064859ded8b3d47dd1549e", null ],
    [ "intersect", "d3/dfc/classrt_1_1_object.html#afa120657cb1976a3356bd269fc2b7e8e", null ],
    [ "send", "d3/dfc/classrt_1_1_object.html#a23ff7804dbed722e35b654736ce5f35e", null ],
    [ "set_appearance", "d3/dfc/classrt_1_1_object.html#ac8e016b6281103f915c1a2d813c87c99", null ],
    [ "set_color", "d3/dfc/classrt_1_1_object.html#a5dba537f276372c52812761befe2ea11", null ],
    [ "set_diffuse", "d3/dfc/classrt_1_1_object.html#a4a8401b294c5dd45a701477a511c5862", null ],
    [ "set_specular", "d3/dfc/classrt_1_1_object.html#afb82d84c7afe377350b5d2d0db62581e", null ],
    [ "set_specular_exponent", "d3/dfc/classrt_1_1_object.html#a5fc8531cf06dd87ca8ab5d21c026d6f6", null ],
    [ "col", "d3/dfc/classrt_1_1_object.html#afe8e295971bf962f4589000787eb0738", null ],
    [ "real", "d3/dfc/classrt_1_1_object.html#a840d278bb6d330cf4027774aacdcd4f0", null ],
    [ "type", "d3/dfc/classrt_1_1_object.html#a248b34b1c4075f3afa8831f8dc7d01c5", null ]
];