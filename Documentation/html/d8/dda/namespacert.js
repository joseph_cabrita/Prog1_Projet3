var namespacert =
[
    [ "Camera", "d4/d5d/classrt_1_1_camera.html", "d4/d5d/classrt_1_1_camera" ],
    [ "color", "da/d56/classrt_1_1color.html", "da/d56/classrt_1_1color" ],
    [ "Hit", "d2/d59/classrt_1_1_hit.html", "d2/d59/classrt_1_1_hit" ],
    [ "image", "dc/d45/classrt_1_1image.html", "dc/d45/classrt_1_1image" ],
    [ "Light", "da/d48/classrt_1_1_light.html", "da/d48/classrt_1_1_light" ],
    [ "Matrix", "d4/d52/classrt_1_1_matrix.html", "d4/d52/classrt_1_1_matrix" ],
    [ "Object", "d3/dfc/classrt_1_1_object.html", "d3/dfc/classrt_1_1_object" ],
    [ "Plane", "d2/dea/classrt_1_1_plane.html", "d2/dea/classrt_1_1_plane" ],
    [ "PointLight", "d3/d5e/classrt_1_1_point_light.html", "d3/d5e/classrt_1_1_point_light" ],
    [ "Ray", "d2/d7f/classrt_1_1_ray.html", "d2/d7f/classrt_1_1_ray" ],
    [ "rect", "de/d76/classrt_1_1rect.html", null ],
    [ "Scene", "d6/d29/classrt_1_1_scene.html", "d6/d29/classrt_1_1_scene" ],
    [ "screen", "d0/dc7/classrt_1_1screen.html", "d0/dc7/classrt_1_1screen" ],
    [ "Sphere", "d6/d6e/classrt_1_1_sphere.html", "d6/d6e/classrt_1_1_sphere" ],
    [ "vector", "d0/d9a/structrt_1_1vector.html", "d0/d9a/structrt_1_1vector" ]
];