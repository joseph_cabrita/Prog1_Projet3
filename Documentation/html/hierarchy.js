var hierarchy =
[
    [ "rt::Camera", "d4/d5d/classrt_1_1_camera.html", null ],
    [ "rt::color", "da/d56/classrt_1_1color.html", null ],
    [ "rt::Hit", "d2/d59/classrt_1_1_hit.html", null ],
    [ "rt::image", "dc/d45/classrt_1_1image.html", [
      [ "rt::screen", "d0/dc7/classrt_1_1screen.html", null ]
    ] ],
    [ "rt::Light", "da/d48/classrt_1_1_light.html", [
      [ "AmbientLight", "d4/dd4/class_ambient_light.html", null ],
      [ "AmbientLight", "d4/dd4/class_ambient_light.html", null ],
      [ "DirectionalLight", "d0/db5/class_directional_light.html", null ],
      [ "DirectionalLight", "d0/db5/class_directional_light.html", null ],
      [ "rt::PointLight", "d3/d5e/classrt_1_1_point_light.html", null ]
    ] ],
    [ "rt::Matrix", "d4/d52/classrt_1_1_matrix.html", null ],
    [ "rt::Object", "d3/dfc/classrt_1_1_object.html", [
      [ "rt::Plane", "d2/dea/classrt_1_1_plane.html", null ],
      [ "rt::Sphere", "d6/d6e/classrt_1_1_sphere.html", null ]
    ] ],
    [ "rt::Ray", "d2/d7f/classrt_1_1_ray.html", null ],
    [ "rt::Scene", "d6/d29/classrt_1_1_scene.html", null ],
    [ "SDL_Rect", null, [
      [ "rt::rect", "de/d76/classrt_1_1rect.html", null ]
    ] ],
    [ "rt::vector", "d0/d9a/structrt_1_1vector.html", null ]
];