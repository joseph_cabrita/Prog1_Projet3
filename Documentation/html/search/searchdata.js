var indexSectionsWithContent =
{
  0: "abcdefghilmnoprstuvwxyz~",
  1: "acdhilmoprsv",
  2: "r",
  3: "acdhilmoprstv",
  4: "abcdfghilmnoprstuvw~",
  5: "bcdgrtwxyz",
  6: "emt",
  7: "d"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "defines",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Macros",
  7: "Pages"
};

