var searchData=
[
  ['ray',['Ray',['../d2/d7f/classrt_1_1_ray.html#aa051777279936e00bda9ebd1eec892d9',1,'rt::Ray::Ray(vector o, vector d, color c)'],['../d2/d7f/classrt_1_1_ray.html#aafa109b4bf906d861ab91dab86aef3b5',1,'rt::Ray::Ray(vector o, vector d)']]],
  ['ray_5forientation',['ray_orientation',['../d4/d5d/classrt_1_1_camera.html#a805ca6b8be4c37b5e578effbb9d0adf1',1,'rt::Camera']]],
  ['ray_5fposition',['ray_position',['../d4/d5d/classrt_1_1_camera.html#a633b9645b88a9d287f0a6095b90fa267',1,'rt::Camera']]],
  ['ray_5ftracing',['ray_tracing',['../d6/d29/classrt_1_1_scene.html#a73240165b6eb8e963493566ac3a21cf8',1,'rt::Scene']]],
  ['ray_5ftracing_5ftest',['ray_tracing_test',['../d6/d29/classrt_1_1_scene.html#ab5a02dfcecdba19de6cd63c9fc9c3ab5',1,'rt::Scene']]],
  ['reflexion',['reflexion',['../d6/d29/classrt_1_1_scene.html#ab1d8174007bb1c5ac34f9631e8c3cb23',1,'rt::Scene']]]
];
