var searchData=
[
  ['scene',['Scene',['../d6/d29/classrt_1_1_scene.html#ab095858b4b50a83f2d6130f56cdaf59a',1,'rt::Scene']]],
  ['screen',['screen',['../d0/dc7/classrt_1_1screen.html#ad3592d93fae438c02bf0811cf948a632',1,'rt::screen']]],
  ['send',['send',['../d3/dfc/classrt_1_1_object.html#a23ff7804dbed722e35b654736ce5f35e',1,'rt::Object::send()'],['../d2/dea/classrt_1_1_plane.html#ae0029022325193b17d5df1dd1e14b0e4',1,'rt::Plane::send()'],['../d6/d6e/classrt_1_1_sphere.html#a7a9b54b21dc054ba57e36c1db3433d41',1,'rt::Sphere::send()']]],
  ['set',['set',['../d4/d52/classrt_1_1_matrix.html#a1f4a75c7d7096b2a97b71f626e495681',1,'rt::Matrix']]],
  ['set_5falpha',['set_alpha',['../da/d56/classrt_1_1color.html#ac30286702b430c1777be58a701de980c',1,'rt::color']]],
  ['set_5fappearance',['set_appearance',['../d3/dfc/classrt_1_1_object.html#ac8e016b6281103f915c1a2d813c87c99',1,'rt::Object']]],
  ['set_5fblue',['set_blue',['../da/d56/classrt_1_1color.html#a998b075bd95b2fe2bf6110d3ea04d381',1,'rt::color']]],
  ['set_5fcolor',['set_color',['../d2/d59/classrt_1_1_hit.html#aa51a76902ed810ea85668ff3114ca46c',1,'rt::Hit::set_color()'],['../d3/dfc/classrt_1_1_object.html#a5dba537f276372c52812761befe2ea11',1,'rt::Object::set_color()']]],
  ['set_5fdiffuse',['set_diffuse',['../d3/dfc/classrt_1_1_object.html#a4a8401b294c5dd45a701477a511c5862',1,'rt::Object']]],
  ['set_5fgreen',['set_green',['../da/d56/classrt_1_1color.html#ac1e391159d2d6290835cfd4f067f5661',1,'rt::color']]],
  ['set_5fis_5flight',['set_is_light',['../d2/d59/classrt_1_1_hit.html#a10d39f827d17a101b4a6ccda3fb8dbc3',1,'rt::Hit']]],
  ['set_5forientation',['set_orientation',['../d4/d5d/classrt_1_1_camera.html#a5c47f88ab4359993577198f9f7d000f3',1,'rt::Camera']]],
  ['set_5fpixel',['set_pixel',['../dc/d45/classrt_1_1image.html#aa24092c4efc9910dfaf2a0a0c42476b8',1,'rt::image']]],
  ['set_5fposition',['set_position',['../d4/d5d/classrt_1_1_camera.html#a7c4ba97d90d01f9ec5d24325c7b30719',1,'rt::Camera']]],
  ['set_5fred',['set_red',['../da/d56/classrt_1_1color.html#a10dcd4b679fc8f54dcfa4b200131d3d4',1,'rt::color']]],
  ['set_5fscreen_5foffset',['set_screen_offset',['../d4/d5d/classrt_1_1_camera.html#ae0a6d9f35816adf5b92ba9b4dc21582c',1,'rt::Camera']]],
  ['set_5fspecular',['set_specular',['../d3/dfc/classrt_1_1_object.html#afb82d84c7afe377350b5d2d0db62581e',1,'rt::Object']]],
  ['set_5fspecular_5fexponent',['set_specular_exponent',['../d3/dfc/classrt_1_1_object.html#a5fc8531cf06dd87ca8ab5d21c026d6f6',1,'rt::Object']]],
  ['simple_5fdraw',['simple_draw',['../d6/d29/classrt_1_1_scene.html#a4bf5f6a1dfbdb60630bc00cbb7d4f6bc',1,'rt::Scene']]],
  ['size',['size',['../d4/d52/classrt_1_1_matrix.html#a7f7341a9cf5aebee101b536721565e0b',1,'rt::Matrix']]],
  ['sphere',['Sphere',['../d6/d6e/classrt_1_1_sphere.html#a30fecc25f909db8cae212d106f1865ec',1,'rt::Sphere']]]
];
