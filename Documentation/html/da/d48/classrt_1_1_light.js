var classrt_1_1_light =
[
    [ "Light", "da/d48/classrt_1_1_light.html#ae1eeff96613bb87ce029339239571d73", null ],
    [ "apply", "da/d48/classrt_1_1_light.html#ac5065027856b9c57891ec7c162298e4f", null ],
    [ "get_color", "da/d48/classrt_1_1_light.html#a6547ec86c966643f58ea2f00b8a78712", null ],
    [ "get_hit_direction", "da/d48/classrt_1_1_light.html#ad8a151ccd59c7ac5a270fc58a3962a93", null ],
    [ "get_type", "da/d48/classrt_1_1_light.html#a6ce99ed9ae5927bc9ace7e5f52dbad2c", null ],
    [ "is_reachable", "da/d48/classrt_1_1_light.html#a8c9fdfd7b8e7e0013f40bf3e3b8bd9e1", null ],
    [ "col", "da/d48/classrt_1_1_light.html#a0203b5206f9d1290a8bde3cc7496239d", null ],
    [ "type", "da/d48/classrt_1_1_light.html#adf73d3ba1c5a0b83e7d6ef438627a71a", null ]
];