/**
 * @file main.cpp
 * @version 1.0
 * @date 08 december 2017
 *
 * @brief Entry point of the program, This file is mainly relevant to 
 * 		  SDL, and serve for demos.
 */

#include <iostream>
#include <vector>

#include "Screen/vector.hpp"
#include "Scene/Objects/object.hpp"
#include "Screen/image.hpp"
#include "Screen/screen.hpp"
#include "Scene/camera.hpp"
#include "Scene/scene.hpp"
#include "Scene/Objects/sphere.hpp"
#include "Scene/Objects/plane.hpp"
#include "Tools/matrix.hpp"
#include "Scene/Lights/AmbientLight.hpp"
#include "Scene/Lights/PointLight.hpp"
#include "Scene/Lights/DirectionalLight.hpp"

using namespace rt;

//Execute tests on demand
#ifdef _TESTS_

#endif

using namespace std;

/**
 * @brief Entry point of the program
 *
 * This function prepare all the SDL environment, and launch the Event 
 * loop.
 * We should catch demo commands (numbers from 0 to 3), and 
 * opportunities to quit properly the program (q and the red cross of 
 * the window).
 *
 * @return 0 if the execution terminate normally, anything else 
 * 		   otherwise.
 */
int main() {

    rt::screen s(800,600);

    cin.exceptions(ios::failbit | ios::badbit);

    Camera* camera = new Camera(rt::vector(100.0, 100.0, 0.0),
                                rt::vector(0.0, 0.0, 1.0), 150);

    Scene* scene = new Scene(camera, s);

    AmbientLight* la = new AmbientLight(rt::color(100, 100, 100));

    PointLight* l1 = new PointLight(rt::vector(0., 200., -100.), rt::color(255, 255, 255));

    PointLight* l2 = new PointLight(rt::vector(320., 240., -150.), rt::color(180, 180, 180));

    DirectionalLight* ld = new DirectionalLight(rt::vector(1., 1., 1.), rt::color(255, 255, 255));

    Sphere sa1(rt::vector(100.0, 0.0, 200.0), 80.0,
               rt::color(255, 0, 255));
    sa1.set_diffuse(0.1);
    sa1.set_specular(0.9);

    Sphere sa2(rt::vector(100.0, 200.0, 100.0), 80.0,
               rt::color(255, 255, 0));
    sa2.set_diffuse(0.8);
    sa2.set_specular(0.9);

    Sphere sa3(rt::vector(-50.0, 200.0, 100.0), 50.0,
               rt::color(0, 255, 255));
    sa3.set_diffuse(0.9);
    sa3.set_specular(0.01);

    Sphere sb1(rt::vector(233.0, 290.0, 100.0), 100.0,
               rt::color(255, 0, 255));
    sb1.set_diffuse(0.5);
    sb1.set_specular(0.9);
    sb1.set_specular_exponent(10);

    Sphere sb2(rt::vector(407.0, 290.0, 100.0), 100.0,
               rt::color(255, 255, 0));
    sb1.set_diffuse(0.5);
    sb1.set_specular(0.9);
    sb1.set_specular_exponent(25.0);

    Sphere sb3(rt::vector(320.0, 140.0, 100.0), 100.0,
               rt::color(0, 255, 255));
    sb1.set_diffuse(0.5);
    sb1.set_specular(0.9);
    sb3.set_specular_exponent(150.);

    Plane pb1(rt::vector(250, 0., 0.),
              rt::vector(0.0, 0.0, 1.0),
              rt::vector(0.0, 1.0, 0.0),
              color(0,255,255));

    Sphere sc1(rt::vector(100.0, 100.0, 100.0), 50.0,
               rt::color(255, 0, 255));

    Sphere sc2(rt::vector(150.0, 50.0, 200.0), 150.0,
               rt::color(127, 127, 255));

    Sphere sc3(rt::vector(50.0, 150.0, 80.0), 20.0,
               rt::color(127, 127, 255));

    SDL_WM_SetCaption("Démo n°2 - Cabrita-Lixon-Lequen", nullptr);

    s.update();

    //Main event Loop
    bool bwait = true;
    SDL_Event event; // NOLINT
    while(bwait)
    {
        SDL_WaitEvent(&event);
        switch(event.type)
        {
            //Close Event
            case SDL_QUIT:
                SDL_Quit();
                bwait = false;
                break;

            //Keyboard Event
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                    case SDLK_0:
                    case SDLK_KP0:
                        SDL_WM_SetCaption("Démo n°2 - Cabrita-Lixon-"
						"Lequen - Green Cross (Test of the correct " 
						"initialization of Video)", nullptr);
						
                        s.fill_rect(0,0,s.width(),s.height(),
									rt::color::BLACK);
                        scene->draw_centered_cross(s, 50, 50, 10,
												  rt::color::GREEN);
                        s.update();
                        break;

                    case SDLK_1:
                    case SDLK_KP1:
                        SDL_WM_SetCaption("Démo n°1 - Cabrita-Lixon-"
                                                  "Lequen - Scene without shadow", nullptr);

                        camera->set_screen_offset(0);
                        scene->draw_shadowless();
                        break;

                    case SDLK_2:
                    case SDLK_KP2:
                        SDL_WM_SetCaption("Démo n°2 - Cabrita-Lixon-"
                                                  "Lequen - Normal map",
                                          nullptr);

                        scene->draw_test(1);
                        break;

                    case SDLK_3:
                    case SDLK_KP3:
                        SDL_WM_SetCaption("Démo n°1 - Cabrita-Lixon-"
                                                  "Lequen - Reflexions Map",
                                          nullptr);

                        scene->draw(0);
                        break;

                    case SDLK_4:
                    case SDLK_KP4:
                        SDL_WM_SetCaption("Démo n°1 - Cabrita-Lixon-"
                                                  "Lequen - Draw Scene",
                                          nullptr);

                        scene->draw_aa(2);
                        break;

                    case SDLK_5:
                    case SDLK_KP5:
                        SDL_WM_SetCaption("Démo n°1 - Cabrita-Lixon-"
                                                  "Lequen - Draw Scene with Anti-Aliasing",
                                          nullptr);

                        scene->draw_aa(3);
                        break;

                    case SDLK_8:
                    case SDLK_KP8:
                        camera->set_screen_offset(350);
                        s.update();
                        break;

                    case SDLK_9:
                    case SDLK_KP9:
                        camera->set_screen_offset(0);
                        s.update();
                        break;

                    case SDLK_PLUS:
                    case  SDLK_KP_PLUS:
                        camera->set_screen_offset(
									camera->get_screen_offset() + 20);
                        break;

                    case SDLK_MINUS:
                    case  SDLK_KP_MINUS:
                        camera->set_screen_offset(
									camera->get_screen_offset() - 20);
                        break;

                    case SDLK_c:
                        camera = new Camera(rt::vector(100.0, 100.0, 0.0),
                                                    rt::vector(0.0, 0.0, 1.0), 150);

                        scene = new Scene(camera, s);

                        scene->add_light((rt::Light*)la);

                        scene->add_light((rt::Light*)l1);

                        scene->add_object(&sa1);

                        scene->add_object(&sa2);

                        scene->add_object(&sa3);

                        scene->add_object(&pb1);

                        break;

                    case SDLK_b:
                        camera = new Camera(rt::vector(320.0, 240.0, 00.0),
                                                    rt::vector(0.0, 0.0, 1.0), 150);

                        scene = new Scene(camera, s);

                        scene->add_light((rt::Light*)la);

                        scene->add_light((rt::Light*)l2);

                        scene->add_object(&sb1);

                        scene->add_object(&sb2);

                        scene->add_object(&sb3);

                        break;

                    case SDLK_a:
                        camera = new Camera(rt::vector(100.0, 100.0, 00.0),
                                                    rt::vector(0.0, 0.0, 1.0), 150);

                        scene = new Scene(camera, s);

                        scene->add_light((rt::Light*)la);

                        scene->add_light((rt::Light*)l1);

                        //scene->add_light((rt::Light*)ld);

                        scene->add_object(&sc1);

                        scene->add_object(&sc2);

                        scene->add_object(&sc3);

                        break;

                    case SDLK_q:
                        SDL_Quit();
                        bwait = false;

                    default:
                        break;
                }
                break;

            default:
                break;
        }
    }

   return 0;
}
