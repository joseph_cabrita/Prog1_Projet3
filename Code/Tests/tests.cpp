/**
 * @file tests.cpp
 * @version 2.0
 * @date 11 december 2017
 * 
 * @brief A test file for matrix.
 */

#include "tests.hpp"

using namespace rt;

Matrix define_m1() 
{
    Matrix m(3);
    m.set(0,0,1);
    m.set(0,1,1);
    m.set(0,2,0);

    m.set(1,0,0);
    m.set(1,1,4);
    m.set(1,2,4);

    m.set(2,0,0);
    m.set(2,1,-2);
    m.set(2,2,8);

    return m;
}

Matrix define_m2() 
{
    Matrix m(3);
    m.set(0,0,1.2);
    m.set(0,1,1.9);
    m.set(0,2,14.5);

    m.set(1,0,4.2);
    m.set(1,1,3.2);
    m.set(1,2,-4);

    m.set(2,0,-10);
    m.set(2,1,-2);
    m.set(2,2,4.8);

    return m;
}

int tests() 
{
    Matrix m = define_m1();
    Matrix i = m.inverse3();

    m.print_matrix();
    cout << m.det3() << " -> " << boolalpha << (m.det3() == 40) << endl;

    i.print_matrix();
    cout << i.det3() << " -> " << boolalpha << (i.det3() == 
												(1.0/m.det3())) << endl;


    return 0;
}

