/**
 * @file tests.cpp
 * @version 2.0
 * @date 11 december 2017
 * 
 * @brief Definition of tests functions.
 */
 
#ifndef _TESTS_H_
#define _TESTS_H_

#include "../Tools/matrix.hpp"
#include <iostream>

using namespace rt;

Matrix define_m();

int tests();

#endif //_TESTS_H_
