/**
 * @file matrix.cpp
 * @version 1.0
 * @date 08 december 2017
 *
 * @brief Implementation file of Matrix class
 */

#include <cassert>
#include <iostream>
#include "matrix.hpp"

using namespace std;


/**
 * Creates a new matrix of size n x n
 * @param n
 */
rt::Matrix::Matrix(int n)
{
    mat = std::vector< std::vector<double> >(n);

    for (unsigned i=0; i < mat.size(); ++i) {
        std::vector<double> col(n);
        mat.at(i) = col;
    }
}


void rt::Matrix::print_matrix(){
    for (unsigned i=0; i<size(); ++i){
        for (unsigned j = 0; j<size();++j) {
            cout << get(i,j) << " ";
        }
        cout << endl;
    }
}


void rt::Matrix::set(int i, int j, double v) {
    mat.at(i).at(j) = v;
}

unsigned long rt::Matrix::size() const
{
    return mat.size();
}


double rt::Matrix::get(int i, int j) const
{
    return mat.at(i).at(j);
}


double rt::Matrix::det3() const
{
    assert(size() == 3);

    double a = (get(2,0)*get(1,1)*get(0,2));
    double b = (get(2,1)*get(1,2)*get(0,0));
    double c = (get(2,2)*get(1,0)*get(0,1));

    double d = (get(0,0)*get(1,1)*get(2,2));
    double e = (get(0,1)*get(1,2)*get(2,0));
    double f = (get(0,2)*get(1,0)*get(2,1));

    return d + e + f - a - b - c;
}


rt::Matrix rt::Matrix::inverse3() const
{
    assert(size() == 3);

    Matrix inv = Matrix(3);

    double det_value = det3();

    for (unsigned i = 0; i < 3; ++i) {
        for (unsigned j = 0; j < 3; ++j) {
            inv.mat.at(i).at(j) =  (1/det_value) * pow(-1, (i+j) % 2)
                              *( get((i+1) % 3, (j+1) % 3) * get((i+2) %
														3, (j+2) % 3)
                                -get((i+1) % 3, (j+2) % 3) * get((i+2) %
														3, (j+1) % 3) );
        }
    }

    return inv;
}

