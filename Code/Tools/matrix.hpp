/**
 * @file matrix.hpp
 * @version 1.1
 * @date 08 december 2017
 *
 * @brief Definitions file of Matrix class
 */
#ifndef _MATRIX_H_
#define _MATRIX_H_

#include <vector>
#include <cmath>
#include "../Screen/vector.hpp"

using namespace std;

namespace rt
{
    /**
     * @class Matrix
     *
     * @brief A class made for using some Matrix.
     *
     * @warning This class works only with square Matrix.
     */
    class Matrix
    {
        public:

            /**
             * @brief Creates a new matrix of size n x n.
             *
             * @param n the size of the Matrix.
             */
            Matrix(int n);

            /**
             * @brief Print the matrix in a console view.
             *
             * @note This function will be ineffective without a console
             * 		 interface.
             */
            void print_matrix();

            /**
             * @brief Set the (i, j) item of the Matrix.
             *
             * @param i The row of the Matrix.
             * @param j The column of the Matrix.
             * @param v The value to insert at this position.
             */
            void set(int i, int j, double v);

            /**
             * @brief Return the size of the matrix.
             *
             * @return The size s of the Matrix (s*s).
             */
            unsigned long size() const;

            /**
             * @brief Give the (i, j) item of the Matrix.
             *
             * @param i The row of the item.
             * @param j The column of the item.
             * @return The item requested.
             */
            double get(int i, int j) const;

            /**
             * @brief Calculate the determinant of the Matrix.
             *
             * @return The determinant of the Matrix.
             *
             * @warning Works only with size 3 Matrix.
             */
            double det3() const;

            /**
             * @brief Invert the Matrix.
             *
             * @return The inverted Matrix.
             *
             * @warning Works only with size 3 Matrix.
             */
            Matrix inverse3() const;

        private:
            std::vector< std::vector<double> > mat;
    };
}


#endif //_MATRIX_H_

