/**
 * @file camera.hpp
 * @version 1.2
 * @date 08 december 2017
 *
 * @brief Definition file of Camera class.
 */

#pragma once

#include <iostream>
#include <cassert>

#include "../Screen/vector.hpp"
#include "../Screen/screen.hpp"

using namespace rt;

namespace rt
{
    /**
     * @class Camera
     *
     * @brief Implementation of a camera, so as to display
     * 		  an image of a Scene.
     *
     * @note The camera system is adapted for working only with 
     * 		 horizontal camera, for simplification purpose.
     */
    class Camera
    {
        public:
            /**
             * @brief Creates a new camera.
             *
             * @deprecated
             *
             * This function creates a new Camera, positioned at
             * (x=300, y=300, z=0), watching in z direction, with an 
             * orthographic projection.
             */
            Camera();

            /**
             * @brief Creates a new camera.
             * @since 1.1
             *
             * @param _position Position of the camera.
             * @param _orientation Direction the camera is pointing to.
             * @param _screen_offset Distance between the camera and the
             * 		  screen. When it is 0, the camera is orthographic. 
             * 		  When is greater than 0, it becomes a perspective 
             * 		  camera.
             */
            Camera(vector _position, vector _orientation, 
												double _screen_offset);

            /**
             * @brief Give the direction of the ray traced from the 
             * 		  pixel (i, j)
             *
             * @param i x coordinate of the pixel.
             * @param j y coordinate of the pixel.
             * @param s screen on which the pixel is to be rendered.
             *
             * @return the direction of the ray traced from the pixel 
             * 		   (i, j).
             */
            vector ray_orientation(double i, double j, screen &s, int ratio = 1);

            /**
             * @brief the position of the pixel (i, j).
             *
             * @param i x coordinate of the pixel.
             * @param j y coordinate of the pixel.
             * @param s screen on which the pixel is to be rendered.
             *
             * @return the position of the pixel (i, j).
             */
            vector ray_position(double i, double j, screen &s, int ratio = 1);

            /**
             * @brief Give the position of the Camera.
             *
             * @return The position of the Camera.
             */
            vector get_position() const;

            /**
             * @brief Give the orientation of the Camera.
             *
             * @return The orientation of the Camera.
             */
            vector get_orientation() const;

            /**
             * @brief Give the screen offset (offset between the Camera 
             * 		  and his screen) of the Camera.
             *
             * @return The screen offset of the Camera.
             */
            double get_screen_offset() const;

            /**
             * @brief Update the position of the Camera.
             *
             * @param _position The new position of the Camera.
             */
            void set_position(vector _position);

            /**
             * @brief Update the orientation of the Camera.
             *
             * @param _orientation The new orientation of the Camera.
             */
            void set_orientation(vector _orientation);

            /**
             * @brief Update the screen offset of the Camera.
             *
             * @param _position The new screen offset of the Camera.
             *
             * @note Giving 0 in parameter will render this camera with 
             * 		 an orthographic projection.
             */
            void set_screen_offset(double _screen_offset);

        private:
            vector position;
            vector orientation;
            double screen_offset;
    };
}

