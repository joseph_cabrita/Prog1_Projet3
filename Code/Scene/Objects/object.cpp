/**
 * @file object.cpp
 * @version 1.0
 * @date 07 december 2017
 *
 * @brief The file who contains all functions implementation of class 
 * 		  Object.
 */
#include "object.hpp"

using namespace rt;

color Object::get_color() const
{
    return col;
}


double Object::get_diffuse() const
{
    return diffuse;
}


double Object::get_specular() const
{
    return specular;
}


double Object::get_specular_exponent() const
{
    return specular_exponent;
}


void Object::set_color(color c)
{
    col = c;
}


void Object::set_diffuse(double diff)
{
    diffuse = diff;
}


void Object::set_specular(double spec)
{
    specular = spec;
}


void Object::set_specular_exponent(double spec_ex)
{
    specular_exponent = spec_ex;
}


void Object::set_appearance(color  c,    double diff,
                            double spec, double spec_ex)
{
    col = c;
    diffuse = diff;
    specular = spec;
    specular_exponent = spec_ex;
}

short Object::get_type()
{
    return type;
}

