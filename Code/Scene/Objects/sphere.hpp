/**
 * @file sphere.hpp
 * @version 2.0
 * @date 11 december 2017
 *
 * @brief This file contains definitions of Sphere class
 */

#ifndef _SPHERE_H_
#define _SPHERE_H_


#include "../../Screen/vector.hpp"
#include "../../Screen/color.hpp"
#include "../ray.hpp"
#include "object.hpp"
#include "../hit.hpp"

#define EPSILON 1e-8

using namespace rt;

namespace rt
{
    /**
     * @class Sphere
     *
     * @brief This class implements the sphere object.
     *
     * @inherit Object
     */
    class Sphere : public Object
    {
        public:
            /**
             * @brief Constructor of the Sphere class.
             *
             * @param c The center point of the Sphere.
             * @param r The radius of the Sphere.
             * @param co The color wanted for the Sphere.
             */
            Sphere(rt::vector c, double r, color co);

            /**
             * @brief Give the center of the Sphere.
             *
             * @return The center of the Sphere.
             */
            vector get_center();

            /**
             * @brief Give the radius of the Sphere.
             *
             * @return The radius of the Sphere.
             */
            double get_radius();

            /**
             * @brief Give the color of the Sphere.
             *
             * @return The color of the Sphere.
             */
            color get_color();

            /**
             * @brief Give the Sphere normal vector at the given 
             * 		  intersection point of the Sphere.
             *
             * @param intersection The point of the Sphere for which we 
             * 		  			   want to find the adapted normal 
             * 					   vector.
             *
             * @return The normal vector requested.
             */
            vector get_normal(const vector& intersection) const;

            /**
             * @brief This function tests if a Ray will intersect the
             * 		  Sphere.
             *
             * @param r The Ray for which we want to know if it 
             * 			intersects the Sphere.
             *
             * @return The distance needed to intersect the Sphere, 
             * 		   infinity if the sphere will not be
             * 		   intersected.
             */
            double send(Ray r);

            /**
             * @brief Give the normal vector of a given point of the 
             * 		  Sphere.
             *
             * @param intersection The point for which we want to know 
             * 					   the normal vector.
             *
             * @return The normal vector of the Sphere, at the given 
             * 		   point.
             */
            Hit intersect(Ray r);

        protected:
            vector center;
            double radius;
    };

}
#endif //_SHPERE_H_

