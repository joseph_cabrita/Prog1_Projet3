/**
 * @file plane.cpp
 * @version 1.2
 * @date 08 december 2017
 *
 * @brief Implementation file of Plane class.
 */

#include "plane.hpp"

using namespace rt;

Plane::Plane(vector pt, vector a1, vector a2, color c) :
    Object(), point(pt), axis1(a1), axis2(a2)
{
    type = 1;
    col = c;
}


Plane::~Plane() {}


color Plane::get_color()
{
    return col;
}


rt::vector Plane::get_normal(const vector &intersection) const
{
    (void) intersection;
    return axis1^axis2;
}


rt::vector Plane::get_point() const
{
    return point;
}


rt::vector Plane::get_first_axis() const
{
    return axis1;
}


rt::vector Plane::get_second_axis() const
{
    return axis2;
}


double Plane::send(Ray r)
{
    if((get_normal(point)|r.get_direction()) > 0)
    {
        return real.infinity();
    }
    else
    {
        vector n = get_normal(point);

        double d = - (n | point);

        double lbd = (- d - (n|r.get_origin())) / (n|r.get_direction());

        if(lbd < 0)
        {
            return real.infinity();
        }
        else
        {
            vector pt_intersect = vector(lbd * r.get_direction().x
                                         + r.get_origin().x,
                                         lbd * r.get_direction().y
                                         + r.get_origin().y,
                                         lbd * r.get_direction().z
                                         + r.get_origin().z);

            vector dist = r.get_origin() - pt_intersect;
            return dist.norm();
        }
    }

}


Hit Plane::intersect(Ray r)
{
    vector n = get_normal(point);

    double d = - (n | point);

    double lbd = (- d - (n|r.get_origin())) / (n|r.get_direction());

    vector pt_intersect = vector(lbd*r.get_direction().x + r.get_origin().x,
                                 lbd*r.get_direction().y + r.get_origin().y,
                                 lbd*r.get_direction().z + r.get_origin().z);

    return Hit(r, pt_intersect, get_normal(point), col);
}


Matrix Plane::define_mp() 
{
    Matrix mat(3);
    mat.set(0,0,axis1.x);
    mat.set(1,0,axis1.y);
    mat.set(2,0,axis1.z);
    mat.set(0,1,axis2.x);
    mat.set(1,1,axis2.y);
    mat.set(2,1,axis2.z);
    mat.set(0,2,point.x);
    mat.set(1,2,point.y);
    mat.set(2,2,point.z);
    return mat;
}

