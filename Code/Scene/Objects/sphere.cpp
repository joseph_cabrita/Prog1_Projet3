/**
 * @file sphere.cpp
 * @version 2.0
 * @date 11 december 2017
 *
 * @brief Object meant to represent a sphere
 */

#include "sphere.hpp"
#include <cmath>
#include <cassert>

using namespace rt;

Sphere::Sphere(vector c, double r, color co) : Object()
{
    type = 0;
    center = c;
    radius = r;
    col = co;
}

rt::vector Sphere::get_center()
{
    return center;
}

double Sphere::get_radius()
{
    return radius;
}

color Sphere::get_color()
{
    return col;
}

/*
 * CONVENTION
 * If the value returned is infinity, then the object is not intersected
 * If the value return is negative, then the ray origin is IN the sphere
 * Else, the value returned is the distance between the origin of the ray
 * and the object
 *
 */
double Sphere::send(Ray r)
{
    rt::vector v    = r.get_direction().unit();
    rt::vector ac   = center - r.get_origin();

    double delta = pow(ac | v, 2.0) - (pow(ac.norm(), 2.0) - 
													pow(radius, 2.0));

    if (delta <= 0)
        return real.infinity();

    double t = (ac | v) - sqrt(delta);
    double s = (ac | v) + sqrt(delta);
    if (t > - EPSILON){
        if (s > - EPSILON)
            t = t < s ? t : s;
        else
            t = -1;
    }else{
        if (s > - EPSILON)
            t = -1;
        else
            t = real.infinity();

    }

    return t;
}

Hit Sphere::intersect(Ray r)
{
    rt::vector v    = r.get_direction().unit();
    rt::vector ac   = center - r.get_origin();

    double delta = pow(ac | v, 2.0)
                   - (pow(ac.norm(), 2.0) - pow(radius, 2.0));

    assert(delta > 0);

    double t = (ac | v) - sqrt(delta);
    double s = (ac | v) + sqrt(delta);

    //We assume both values are positive
    t = t < s ? t : s;

    vector intersection = r.get_origin() + t*v;

    Hit h = Hit(Ray(r), intersection, get_normal(intersection), col);

    return h;

}

rt::vector Sphere::get_normal(const vector &intersection) const
{
    return intersection - center;
}
