/**
 * @file plane.hpp
 * @version 1.1
 * @date 08 december 2017
 *
 * @brief Definition file of class Plane.
 */

#ifndef _PLANE_HPP_
#define _PLANE_HPP_

#include "../../Screen/vector.hpp"
#include "../../Screen/color.hpp"
#include "../ray.hpp"
#include "object.hpp"
#include "../hit.hpp"
#include "../../Tools/matrix.hpp"
#include <limits>

using namespace rt;

namespace  rt 
{
	/**
	 * @class Plane
	 * 
	 * @brief The class who represent the plane object.
	 */
    class Plane : public Object
    {
        public:
            /**
             * @brief Constructor of the Plane class.
             *
             * @param pt The base point of the Plane.
             * @param a1 The first vector defining the Plane.
             * @param a2 The second vector defining the Plane.
             * @param co The color wanted for the Plane.
             */
            Plane(vector pt, vector a1, vector a2, color c);

            /**
             * @brief Default destructor for the Plane Object.
             */
            ~Plane();

            /**
             * @brief Give the color of the Plane.
             *
             * @return The color of the Plane.
             */
            color get_color();

            /**
             * @brief Give the base point of the Plane.
             * @since 1.1
             *
             * @return The color of the Plane.
             */
            vector get_point() const;

            /**
             * @brief Give the first axis of the Plane.
             * @since 1.1
             *
             * @return The first axis of the Plane.
             */
            vector get_first_axis() const;

            /**
             * @brief Give the second axis of the Plane.
             * @since 1.1
             *
             * @return The second axis of the Plane.
             */
            vector get_second_axis() const;

            /**
             * @brief Give the Plane normal vector.
             *
             * @param intersection The point of the Plane for which
             * we want to find the adapted normal vector.
             *
             * @return The normal vector requested.
             */
            vector get_normal(const vector &intersection) const;

            /**
             * @brief This function tests whether a Ray will intersect the Plane.
             *
             * @param r The Ray for which we want to know if it intersects the
             * Plane.
             *
             * @return The distance needed to intersect the Plane, infinity if
             * the sphere will not be intersected.
             */
            double send(Ray r);

            /**
             * @brief Give the normal vector of a given point of the Plane.
             *
             * @param intersection The point for which we want to know the
             * normal Plane.
             *
             * @return The normal vector of the Plane, at the given point.
             */
            Hit intersect(Ray r);

        private:
            vector point;
            vector axis1;
            vector axis2;

            Matrix define_mp();

    };

}
#endif //_PLANE_HPP_

