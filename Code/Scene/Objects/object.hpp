/**
 * @file object.hpp
 * @version 1.1
 * @date 07 december 2017
 *
 * @brief File who implement all definitions of the object class.
 *
 * @warning This class is a virtual class. she need to be inherited for 
 * 			being used.
 */

#pragma once

#include "../../Screen/vector.hpp"
#include "../../Screen/color.hpp"
#include "../hit.hpp"
#include <limits>

using namespace rt;

namespace rt
{
    /**
     * @class Object
     *
     * @brief Basic class for objects representation in the game engine.
     *
     * This class gives a basic interface for all objects of the game.
     * Only inherited object of this class can be rendered by the
     * engine.
     */
    class Object
    {
        public:
            /**
             * @brief This function returns the Hit that corresponds to the
             *		  intersection between the Ray and the Object.
             *
             * @param r The Ray that needs to intersect with the Object.
             *
             * @return The hit generated by the contact between the Ray 
             * 		   and the Object.
             *
             * @warning It is really important to test if the ray will 
             * 			intersect the object. If not, the intersect 
             * 			function can fail.
             */
            virtual Hit intersect(Ray r) = 0;

            /**
             * @brief This function tests if a Ray will intersect the
             * 		  Object.
             *
             * @param r The Ray for which we want to know if it 
             * 			intersects the Object.
             *
             * @return The distance needed to intersect the Object, 
             * 		   infinity if the object won't be intersected.
             */
            virtual double send(Ray r) = 0;

            /**
             * @brief Give the normal vector of a given point of the 
             * 		  Object.
             *
             * @param intersection The point for which we want to know 
             * 		  			   the normal vector.
             *
             * @return The normal vector of the Object, at the given 
             * 		   point.
             */
            virtual vector get_normal(const vector &intersection) const
						= 0;

            // Get methods
            /**
             * @brief Give the color of the Object.
             *
             * @return The color of the Object.
             */
            color get_color() const;

            /**
             * @brief Give the diffuse coefficient of the Object.
             *
             * @return The diffuse coefficient of the Object.
             */
            double get_diffuse() const;

            /**
             * @brief Give the reflection coefficient of the Object.
             *
             * @return The reflection coefficient of the Object.
             */
            double get_specular() const;

            /**
             * @brief Give the specular exponent of the Object.
             *
             * @return The specular exponent of the Object.
             */
            double get_specular_exponent() const;

            // Set methods
            /**
             * @brief Update the color of the Object.
             *
             * @param c The color of the Object.
             *
             * @see set_appearance
             */
            void set_color(color c);

            /**
             * @brief Update the reflection coefficient of the Object.
             *
             * @param diff The reflection coefficient of the Object.
             *
             * @see set_appearance
             */
            void set_diffuse(double diff);

            /**
             * @brief Update the reflection coefficient of the Object.
             *
             * @param spec The reflection coefficient of the Object.
             *
             * @see set_appearance
             */
            void set_specular(double spec);

        /**
             * @brief Update the specular exponent of the Object.
             *
             * @param spec_ex The specular exponent of the Object.
             *
             * @see set_appearance
             */
            void set_specular_exponent(double spec_ex);

            // General set method
            /**
             * @brief Define globally all lightning properties of the 
             * 		  Object.
             *
             * @param c The color of the Object.
             * @param diff The diffusion coefficient of the Object.
             * @param spec The reflection coefficient of the Object.
             * @param spec_ex The specular exponent of the Object.
             *
             * This function is a mix of all set_* functions.
             *
             * @see set_color
             * @see set_difuse
             * @see set_specular
             * @see set_specular_exponent
             */
            void set_appearance(color c, double diff, double spec, 
													  double spec_ex);

            /**
             * @brief Type of the object. 0 for a sphere, 1 for a plane
             *
             * @return The id of the type of the object
             */
            short get_type();

        protected:
            color col;
            std::numeric_limits<double> real;
            short type;

        private:
            double diffuse = 0.5;
            double specular = 0.5;
            double specular_exponent = 10.;

    };
}

