/**
 * @file ray.cpp
 * @version 1.0
 * @date 07 december 2017
 *
 * @brief This file contain the implementation of the Ray class.
 */

#include "ray.hpp"

using namespace rt;

Ray::Ray(vector o, vector d, color c) :
    orig(o), dir(d), col(c)
{}


Ray::Ray(vector o, vector d):
    orig(o),dir(d)
{}


vector Ray::get_origin()
{
    return orig;
}


vector Ray::get_direction()
{
    return dir;
}


color Ray::get_color()
{
    return col;
}

