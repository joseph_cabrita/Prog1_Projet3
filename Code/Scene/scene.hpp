/**
 * @file scene.hpp
 * @version 1.1
 * @date 08 december 2017
 *
 * @brief Definition file of class Scene.
 */

#ifndef _SCENE_H_
#define _SCENE_H_

#include <vector>
#include "../Screen/screen.hpp"
#include "Objects/object.hpp"
#include "camera.hpp"
#include "Lights/Light.hpp"

#define EPSILON 1e-8

namespace rt
{
	/**
	 * @class Scene
	 *
	 * @brief This class represents a full scene, including objects,
	 * 		  cameras, lights, and a screen.
	 */
    class Scene
    {
		public:
            /**
             * @brief Constructor of Scene class.
             *
             * @param _camera The camera used for viewing the Scene.
             * @param s The screen showed by the Camera.
             */
			Scene(Camera* _camera, screen& s);

            /**
             * @brief Draw a square in the given screen.
             *
             * @param s The screen on which we want to draw the square.
             * @param x The horizontal position of the square (we
             * 			considering top left corner in this case).
             * @param y The vertical position of the square (we
             * 			considering top left corner in this case).
             * @param width The size of the square.
             * @param c the color of the square.
             */
			void draw_square(rt::screen& s, int x, int y, int width, rt::color c);

            /**
             * @brief Draws a cross centered on the given positions.
             *
             * @note Mainly used to test whether the SDL is correctly
             * 		 loaded or not.
             *
             * @param s The screen in which we want to draw the cross.
             * @param x The x position of the cross.
             * @param y The y position of the cross.
             * @param width The size of the cross.
             * @param c The color wanted for the cross.
             */
			void draw_centered_cross(rt::screen& s, int x, int y,
												int width, rt::color c);

            /**
             * @brief Draws the scene using only one color.
             *
             * If a ray has encountered an object, the associated pixel
             * becomes white. Else, it is left black.
             */
			void simple_draw();

            /**
             * @brief Draws the scene using the given camera.
             *
             * @note All colors of objects are respected.
             */
			void draw_shadowless();

            /**
             * @brief Draw the scene with all possibilities of the
             * 		  engine, except post-processing
             *
             * @param depth The number of reflections a ray will do before fading
             *
             */
			void draw(int depth);

            /**
             * @brief Draw the scene using anti aliasing
             *
             * @param depth The number of reflections a ray will do before fading
             *
             * @note The use of super sampling may take up to four times
             *       longer to compute the image
             */
            void draw_aa(int depth);


            /**
             * @brief Draw the scene so as to show debug information
             *
             * @param test_id the id of the test : 1 for normals,
             * 2 for reflections
             */
            void draw_test(int test_id);

            /**
             * @brief Add an Object to the Scene.
             *
             * @param object The Object to add.
             */
			void add_object(Object* object);

            /**
             * @brief Add a Light to the Scene.
             *
             * @param light The light to add.
             */
			void add_light(Light* light);

            /**
             * @brief Execute the launch of a given Ray.
             *
             * @param r The Ray we need to launch.
             *
             * @return The color of this Ray launch.
             */
            color ray_tracing(Ray r, int depth = 0);

            /**
             * Clone of the raytracing function, but renders test data
             *
             * @param r The Ray we need to launch.
             * @param test_id The id of the test.
             *
             * @return The color of this Ray launch.
             */
            rt::color ray_tracing_test(Ray r, int test_id, int depth = 0);

            /**
             * @brief Compute the direction of the reflected ray.
             *
             * @param r The Ray for which we want a reflexion.
             * @param obj The object that will reflect the Ray.
             *
             * @return The direction of the reflected Ray.
             */
            rt::vector reflexion(Hit h);

            /**
             *  @brief Given a ray, this will compute the next hit on
             * 		   the next object.
             *
             *  If there is no object on the trajectory of the ray, it
             * 	becomes a black hit on the origin.
             *
             *  @param r The ray for which we want to find the next
             * 			 impact point.
             *
             *  @return The next impact point.
             */
			Hit next_hit(Ray r);

            /**
             * @brief Give the nearest object in the Ray direction.
             *
             * @param r The ray for which we want to find the nearest
             * 			object.
             *
             * @return The nearest object if it exist, nullptr
             * 		   otherwise.
             */
			Object* get_closest_object(Ray r);

            /**
             * @brief Test if a given light is accessible from a given
             * 		  object.
             *
             * @param l The light we want to test.
             * @param h The hit for which we want to find the
             * 			accessibility of the light.
             *
             * @return true if a direct way exist between the light and
             * 		   the object, false otherwise.
             */

    private:
			screen s;
			Camera* camera;
			std::vector<Object*> objects;
			std::vector<Light*> lights;

			/**
			 * @brief Fully clean the Scene screen with a black
			 * 		  background.
			 */
			void clean();
    };
}

#endif //_SCENE_H_

