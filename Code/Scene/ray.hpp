/**
 * @file ray.hpp
 * @version 1.1
 * @date 07 december 2017
 *
 * @brief This file contains all the definitions of the Ray class
 */

#ifndef _RAY_H_
#define _RAY_H_

#include "../Screen/vector.hpp"
#include "../Screen/color.hpp"

namespace rt
{
    /**
     * @class Ray
     *
     * @brief This class represents a light Ray.
     *
     * This class contains properties of a Ray, including its color.
     */
    class Ray
    {
        public:
            /**
             * @brief Constructor of the Ray class.
             *
             * @param o The original point of the Ray.
             * @param d The direction in the space of the Ray.
             * @param c The color of the Ray.
             *
             * We use a color parameter because we work with colored 
             * light rays.
             *
             * @see Ray(vector o, vector d)
             */
            Ray(vector o, vector d, color c);

            /**
             * @brief Constructor of the Ray class.
             *
             * @param o The original point of the Ray.
             * @param d The direction in the space of the Ray.
             *
             * This initialization does not require a color and will 
             * initialize the Ray color to white.
             *
             * @see Ray(vector o, vector d, color c)
             */
            Ray(vector o, vector d);

            /**
             * @brief Give the original position of the Ray.
             *
             * @return The original position of the Ray.
             */
            vector get_origin();

            /**
             * @brief Give the direction of the Ray.
             *
             * @return The direction of the Ray.
             */
            vector get_direction();

            /**
             * @brief Give the color of the Ray.
             *
             * @return The color of the Ray.
             *
             * @note If the color was not given during initialization of
             * 		 the Ray, this function will return a white color.
             */
            color get_color();

        private:
            vector orig;
            vector dir;
            color col;
    };
}

#endif //_RAY_H_

