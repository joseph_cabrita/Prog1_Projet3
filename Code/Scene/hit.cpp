/**
 * @file hit.cpp
 * @version 1.1
 * @date 07 december 2017
 *
 * @brief This file contains the implementation of the Hit class.
 */

#include "hit.hpp"

using namespace rt;

Hit::Hit(Ray g, vector p, vector n, color c, bool l) :
    gen(g), point(p), normal(n), col(c), light(l)
{

}


color Hit::get_color() const
{
    return col;
}


Ray Hit::get_ray()  const
{
    return gen;
}


vector Hit::get_point() const
{
    return point;
}


vector Hit::get_normal() const
{
    return normal;
}


void Hit::set_color(const color &c) {
    col=c;
}


Hit::~Hit()
{

}
