/**
 * @file camera.cpp
 * @version 1.2
 * @date 08 december 2017
 *
 * @brief Implementation file of Camera class
 */
#include "camera.hpp"
#include "ray.hpp"

#define TRANSLAT_FACTOR 0.5


Camera::Camera() :
        position(300.0, 300.0, 0.0),
        orientation(0.0, 0.0, 1.0), screen_offset(0)
{}


Camera::Camera(vector _position, vector _orientation, 
											double _screen_offset)
{
    position = _position;
    vector new_orientation =
            vector(0, _orientation.y, _orientation.z);
    /*
     * The x coordinate must be zero
     * and at least one other coordinate
     * must be different from zero
     */
    assert(new_orientation.norm() != 0);
    orientation = new_orientation.unit();

    screen_offset = _screen_offset;
}


vector Camera::ray_orientation(double i, double j, screen &s, int ratio)
{
    if (screen_offset == 0){
        return orientation;
    }

    rt::vector ray_dir(
        orientation.x * screen_offset + (j-s.height()/2)* 
				TRANSLAT_FACTOR / ratio,
        orientation.y * screen_offset + (i-s.width()/2) * 
				TRANSLAT_FACTOR / ratio * orientation.z,
        orientation.z * screen_offset - (i-s.width()/2) * 
				TRANSLAT_FACTOR / ratio * orientation.y
    );

    return ray_dir.unit();
}


vector Camera::ray_position(double i, double j, screen &s, int ratio)
{
    rt::vector ray_pos(
        position.x + (j-s.height()/2)* TRANSLAT_FACTOR / ratio,
        position.y + (i-s.width()/2) * TRANSLAT_FACTOR / ratio * orientation.z,
        position.z - (i-s.width()/2) * TRANSLAT_FACTOR / ratio * orientation.y
    );

    return ray_pos;
}


void Camera::set_orientation(vector _orientation)
{
    vector new_orientation =
            vector(0, _orientation.y, _orientation.z);
    /*
     * The x coordinate must be zero
     * and at least one other coordinate
     * must be different from zero
     */
    assert(new_orientation.norm() != 0);
    orientation = new_orientation.unit();
}


vector Camera::get_position() const
{
    return position;
}


vector Camera::get_orientation() const
{
    return orientation;
}


double Camera::get_screen_offset() const
{
    return screen_offset;
}


void Camera::set_position(vector _position)
{
    position = _position;
}


void Camera::set_screen_offset(double _screen_offset)
{
    screen_offset = _screen_offset;
}

