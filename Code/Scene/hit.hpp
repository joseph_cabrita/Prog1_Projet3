/**
 * @file hit.hpp
 * @version 1.2
 * @date 08 december 2017
 *
 * @brief This file contains the definition of the Hit class.
 */

#ifndef _HIT_H_
#define _HIT_H_

#include "../Screen/vector.hpp"
#include "../Screen/color.hpp"
#include "ray.hpp"

using namespace rt;
namespace rt
{
    /**
     * @class Hit
     * @brief Represent an intersection between an object and a Ray
     *
     * This class is implemented to give a common interface for all hit 
     * between objects and rays. Hit contains all the datas linked 
     * directly to the hit.
     */
    class Hit
    {
        public:
            /**
             * @brief Constructor of the Hit Class.
             *
             * @param g The Ray that will intersect the Object.
             * @param p The position of the impact between the Hit and 
             * 			the Object.
             * @param n A vector normal to the impact point of the 
             * 			Object.
             * @param c The color of the Ray, after the impact between 
             * 			the original Ray and the Object.
             * @param l true if the impacted object is a light, false 
             * 			otherwise.
             *
             * A Hit made with this system contains all required data to
             * be able to treat the hit in the rest of the game engine.
             */
            Hit(Ray g, vector p, vector n, color c, bool l = false);

            /**
             * @brief Default destructor of the Hit class.
             */
            virtual ~Hit();

            /**
             * @brief Give the Ray who will hit the Object.
             *
             * @return The Ray who will hit the Object.
             */
            Ray get_ray() const;

            /**
             * @brief Give the impact point of the Hit.
             *
             * @return The impact point of the Hit.
             */
            vector get_point() const;

            /**
             * @brief Give the normal vector to the plane tangent to the
             * 		  impact point and the Object.
             *
             * @return The normal vector to the plane tangent to the 
             * 		   impact point and the Object.
             */
            vector get_normal() const;

            /**
             * @brief Give the color of the Ray after the Hit.
             *
             * @return The color of the Ray after the Hit.
             */
            color get_color() const;

            /**
             * @brief Set the color of the Ray after the Hit.
             *
             * @since 1.1
             *
             * @param The color of the Ray after the Hit.
             */
            void set_color(const color& c);

        private:
            Ray gen;
            vector point;
            vector normal;
            color col;

            bool light;
    };
}

#endif //_HIT_H_

