/**
 * @file Light.hpp
 * @version 1.0
 * @date 08 december 2017
 *
 * @brief This file contains the definition of Light class.
 */

#ifndef _LIGHT_H_
#define _LIGHT_H_

#include "../../Screen/vector.hpp"
#include "../../Screen/color.hpp"
#include "../Objects/sphere.hpp"
#include "../hit.hpp"
#include "../Objects/object.hpp"

#define EPSILON 0.01

namespace rt
{
    class Scene; 
    /**
     * @class Light
     *
     * @brief Implement the system of light for the Scene.
     */
    class Light
    {
        public:

            /**
             * @brief Constructor of the Light class.
             *
             * @param p The position of the Light.
             * @param c The color of the Light.
             */
            Light(color c);

            /**
             * @brief Give the color of the light.
             *
             * @return Color of the light.
             **/
            color get_color();

            /**
             * @brief Update the color of a Ray after a Hit.
             *
             * @param r The Hit that just happened.
             * @return The new color of the Ray.
             */
            color apply(Hit r);

            /**
             * @brief Checks whether the hit is reachable by the light 
             * 		  source or not.
             *
             * @param scene Scene where the hit occurred and the light 
             * 				is.
             * @param h Hit that happened.
             * @return True if the hit can be reached by light.
             */
            virtual bool is_reachable(Scene* scene, Hit h) = 0;

            /**
             * @brief Computes the direction from the hit to the light.
             *
             * @param h Hit.
             * @return A unit vector, which is the direction from the 
             * 		   hit to the light.
             */
            virtual rt::vector get_hit_direction(Hit h) = 0;

            /**
             * @brief Returns the type of the light : 0 for ambient, 
             * 		  1 for point light, 2 for directional.
             * @return The id of the type of the light.
             */
            short get_type();

    protected:
            color col;
            short type;
    };
}
#endif //_LIGHT_H_

