/**
 * @file DirectionalLight.hpp
 * @version 2.0
 * @date 11 december 2017
 *
 * @brief Definition file of DirectionalLight class.
 */

#ifndef _DIRECTIONALLIGHT_H_
#define _DIRECTIONALLIGHT_H_

#include "Light.hpp"
#include "../scene.hpp"

/**
 * @class DirectionalLight
 *
 * @brief An directional light who provide a directed light for objects.
 *
 * @inherit Light
 */
class DirectionalLight : public Light
{
    public:
        /**
         * @brief Constructor of the DirectionalLight class.
         *
         * @param p The position of the Light.
         * @param c The color of the Light.
         */
        DirectionalLight(const rt::vector &d, const color &c);

        /**
         * @brief Checks whether the hit is reachable by the light 
         * 		  source or not.
         *
         * @param scene Scene where the hit occurred and the light is.
         * @param h Hit that happened.
         * @return True if the hit can be reached by light.
         */
        bool is_reachable(Scene* scene, Hit h);

        /**
         * @brief Computes the direction from the hit to the light.
         *
         * @param h Hit.
         * @return A unit vector, which is the direction from the hit to 
         * 		   the light.
         */
        rt::vector get_hit_direction(Hit h);

    private:
        rt::vector direction;
};

#endif //_DIRECTIONALLIGHT_H_

