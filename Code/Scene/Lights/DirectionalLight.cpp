/**
 * @file DirectionalLight.cpp
 * @version 2.0
 * @date 11 december 2017
 *
 * @brief Implementation file of DirectionalLight class.
 */

#include "DirectionalLight.hpp"

DirectionalLight::DirectionalLight(const rt::vector &d, const color &c) 
	: Light(c)
{
    direction = d.unit();
    type = 2;
}


bool DirectionalLight::is_reachable(Scene* scene, Hit h)
{
    Ray r(h.get_point(), rt::vector() - direction);

    Object* closest_object = scene->get_closest_object(r);

    if(closest_object == nullptr)
        return true;

    return (h.get_normal() | direction) <= 0;

}


rt::vector DirectionalLight::get_hit_direction(Hit h)
{
    return rt::vector() - direction;
}

