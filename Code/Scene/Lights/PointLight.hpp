/**
 * @file PointLight.hpp
 * @version 2.0
 * @date 11 december 2017
 *
 * @brief Definition file of PointLight class.
 */

#ifndef _POINTLIGHT_H_
#define _POINTLIGHT_H_

#include "Light.hpp"
#include "../scene.hpp"

namespace rt
{
    /**
     * @class PointLight
     *
     * @inherit Light
     *
     * @brief A punctual Light for lighting objects.
     */
    class PointLight : public Light
    {

        public:
            /**
             * @brief Constructor of the PointLight class.
             *
             * @param p The position of the Light.
             * @param c The color of the Light.
             */
            PointLight(vector p, color c);

            /**
             * @brief Checks whether the hit is reachable by the light 
             * 		  source or not.
             *
             * @param scene Scene where the hit occurred and the light 
             * 				is.
             * @param h Hit that happened.
             * @return True if the hit can be reached by light.
             */
            bool is_reachable(Scene* scene, Hit ht);

            /**
             * @brief Computes the direction from the hit to the light.
             *
             * @param h Hit.
             * @return A unit vector, which is the direction from the 
             * 		   hit to the light.
             */
            rt::vector get_hit_direction(Hit h);


        private:
            rt::vector position;
    };
}

#endif //_POINTLIGHT_H_

