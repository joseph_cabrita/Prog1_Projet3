/**
 * @file AmbientLight.cpp
 * @version 2.0
 * @date 11 december 2017
 *
 * @brief Implementation file of AmbientLight class.
 */

#include "AmbientLight.hpp"

AmbientLight::AmbientLight(const color &c) : Light(c)
{
    type = 0;
}


bool AmbientLight::is_reachable(Scene* scene, Hit h)
{
    (void) scene;
    (void) h;
    return true;
}


rt::vector AmbientLight::get_hit_direction(Hit h)
{
    return rt::vector() - h.get_ray().get_direction();
}

