/**
 * @file AmbientLight.hpp
 * @version 2.0
 * @date 11 december 2017
 *
 * @brief Definition file of AmbientLight class.
 */

#ifndef _AMBIENTLIGHT_H_
#define _AMBIENTLIGHT_H_

#include "Light.hpp"
#include "../scene.hpp"

/**
 * @class AmbientLight
 *
 * @brief An ambient light who light up each object as same as each 
 * 		  other.
 *
 * @inherit Light
 */
class AmbientLight : public Light
{
    public:
        /**
         * @brief Constructor of the AmbientLight class.
         *
         * @param p The position of the Light.
         * @param c The color of the Light.
         */
        AmbientLight(const color &c);

        /**
         * @brief Checks whether the hit is reachable by the light 
         * 		  source or not.
         *
         * @param scene Scene where the hit occurred and the light is.
         * @param h Hit that happened.
         * @return True if the hit can be reached by light.
         */
        bool is_reachable(Scene* scene, Hit h);

        /**
         * @brief Computes the direction from the hit to the light.
         *
         * @param h Hit.
         * @return A unit vector, which is the direction from the hit to 
         * 		   the light.
         */
        rt::vector get_hit_direction(Hit h);
};

#endif //_AMBIENTLIGHT_H_

