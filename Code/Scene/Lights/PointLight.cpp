/**
 * @file PointLight.cpp
 * @version 2.0
 * @date 11 december 2017
 *
 * @brief Implementation file of PointLight class.
 */

#include "PointLight.hpp"

PointLight::PointLight(vector p, color c) : Light(c), position(p)
{
    type = 1;
}


bool PointLight::is_reachable(Scene* scene, Hit ht)
{
    vector direction = ht.get_point() - position;

    Ray r(position, direction, color(255, 255, 255));

    Object* obj = scene->get_closest_object(r);

    if (obj == nullptr)
        return true;

    if (obj->send(r) >= direction.norm() - EPSILON)
        return (ht.get_normal() | direction) < 0;

    return false;
}


rt::vector PointLight::get_hit_direction(Hit h)
{
    return (position - h.get_point()).unit();
}

