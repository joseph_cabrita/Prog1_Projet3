/**
 * @file Light.cpp
 * @version 1.0
 * @date 08 december 2017
 * 
 * @brief Implementation file of Light class.
 */

#include "Light.hpp"
#include <cmath>

using namespace rt;

Light::Light(color c) : col(c) {}


color Light::get_color() 
{
    return col;
}


color Light::apply(Hit r) 
{
    vector n     = r.get_normal();
    color  obj_c = r.get_color();

    Ray    g = r.get_ray();
    vector d = g.get_direction();
    double theta = std::abs((d | n)/(d.norm()*n.norm())); 
    
    //Addition of the two colors
    int nr1 = int(sqrt(obj_c.get_red()   * col.get_red()));
    int nb1 = int(sqrt(obj_c.get_blue()  * col.get_blue()));
    int ng1 = int(sqrt(obj_c.get_green() * col.get_green()));

    //Luminosity is adjusted consequently
    int nr2 = int(nr1 * theta);
    int nb2 = int(nb1 * theta);
    int ng2 = int(ng1 * theta);

    color nc(nr2, ng2, nb2);

    return nc;

}

short Light::get_type()
{
    return type;
}

