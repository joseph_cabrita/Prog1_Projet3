/**
 * @file scene.cpp
 * @version 2.0
 * @date 11 december 2017
 * 
 * @brief Implementation file of Scene class
 */

#include <limits>
#include <cmath>
#include "scene.hpp"

#define MIN(a, b) (a < b) ? a : b

#define TRANSLAT_FACTOR .5

using namespace rt;

Scene::Scene(Camera* _camera, screen& s) : s(s)
{
    camera = _camera;
}


void Scene::draw_square(rt::screen& s, int x, int y, int width,
														rt::color c)
{
    s.fill_rect(x, y, x + width - 1, y + width - 1, c);
}


void Scene::draw_centered_cross(rt::screen& s, int x, int y, int width,
															rt::color c)
{
    assert(width > 0);
    int my_x = x - width/2;
    int my_y = y - width/2;
    draw_square(s, my_x, my_y, width, c);
    draw_square(s, my_x + width, my_y, width, c);
    draw_square(s, my_x - width, my_y, width, c);
    draw_square(s, my_x, my_y + width, width, c);
    draw_square(s, my_x, my_y - width, width, c);
    s.set_pixel(x, y, rt::color::RED);
}


void Scene::simple_draw()
{
    clean();

    std::numeric_limits<double> real;

    for (int i = 0; i < s.width(); ++i)
    {
        for (int j = 0; j < s.height(); ++j)
        {
            for(unsigned int k = 0; k < objects.size(); ++k)
            {
                rt::vector campos = camera->get_position();
                rt::vector camor  = camera->get_orientation();

                rt::vector pos(
                        campos.x + (j-(s.height()/2))* TRANSLAT_FACTOR,
                        campos.y + (i-(s.width()/2)) * TRANSLAT_FACTOR * camor.z,
                        campos.z - (i-(s.width()/2)) * TRANSLAT_FACTOR * camor.y
                );
                if(real.infinity() != objects[k]->send(Ray(pos, camor)))
                {
                    s.fill_rect(i,j,i+1,j+1,rt::color(255,255,255));
                }
            }
        }
    }
    s.update();
}


void Scene::draw_shadowless()
{
    clean();

    for (int i = 0; i < s.width(); ++i)
    {
        for (int j = 0; j < s.height(); ++j)
        {
            rt::Ray r = Ray(camera->ray_position(i, j, s),
                            camera->ray_orientation(i, j, s));

            Hit h = next_hit(r);

            rt::color draw_color = h.get_color();

            s.fill_rect(i, j, i+1, j+1, draw_color);
        }
    }
    s.update();

}


void Scene::draw(int depth)
{
    clean();

    for (int i = 0; i < s.width(); ++i)
    {
        for (int j = 0; j < s.height(); ++j)
        {
            rt::Ray r = Ray(camera->ray_position(i, j, s),
                            camera->ray_orientation(i, j, s));

            color draw_color = ray_tracing(r, depth);

            s.fill_rect(i, j, i+1, j+1, draw_color);
        }
    }
    s.update();
}


void Scene::draw_aa(int depth)
{
    clean();

    for (int i = 0; i < s.width(); ++i)
    {
        for (int j = 0; j < s.height(); ++j)
        {
            int r = 0;
            int g = 0;
            int b = 0;

            for (int k = 0; k < 2; ++k)
            {
                for (int l = 0; l < 2; ++l)
                {
                    rt::Ray ray = Ray(camera->ray_position(i + k/2., j + l/2., s),
                                      camera->ray_orientation(i + k/2., j + l/2., s));

                    color draw_color = ray_tracing(ray, depth);

                    r += draw_color.get_red();
                    g += draw_color.get_green();
                    b += draw_color.get_blue();
                }
            }

            r /= 4;
            g /= 4;
            b /= 4;


            s.fill_rect(i, j, i+1, j+1, color(r, g, b));
        }
    }
    s.update();
}

void Scene::draw_test(int test_id)
{
    clean();

    for (int i = 0; i < s.width(); ++i)
    {
        for (int j = 0; j < s.height(); ++j)
        {
            rt::Ray r = Ray(camera->ray_position(i, j, s),
                            camera->ray_orientation(i, j, s));

            color draw_color = ray_tracing_test(r, test_id, 1);

            s.fill_rect(i, j, i+1, j+1, draw_color);
        }
    }
    s.update();
}

rt::color Scene::ray_tracing_test(Ray r, int test_id, int depth)
{
    Hit ht = next_hit(r);

    rt::vector hitnorm = ht.get_normal().unit();

    if (test_id == 1)
    {
        rt::color colnorm  = color(std::abs(hitnorm.x) * 255,
                                   std::abs(hitnorm.y) * 255,
                                   std::abs(hitnorm.z) * 255);

        return colnorm;
    }
    if (test_id == 2)
    {
        Object* closest_object = get_closest_object(r);

        /* If the ray doesn't hit any object, then the color of the infinity
           is black */
        if (closest_object == nullptr)
            return color(0, 0, 0);

        if (depth == 0)
            return color(120, 120, 120);

        color  obj_color = closest_object->get_color();
        rt::vector reflected_ray_direction = reflexion(ht);

        color current_color(120, 0, 0);

        double new_red   = 0;
        double new_green = 0;
        double new_blue  = 0;

        // The following part handles reflection
        if (depth > 0)
        {
            // We consider the hit is slightly outside the sphere to avoid collisions
            rt::Ray reflected_ray(ht.get_point() + reflected_ray_direction*EPSILON, reflected_ray_direction);

            // Color of the ray that comes from the light and goes to the object
            color next_color = ray_tracing_test(reflected_ray, 2, depth - 1);

            double ks = closest_object->get_specular();

            new_red   += current_color.get_red()   + next_color.get_red()   * ks;
            new_green += current_color.get_green() + next_color.get_green() * ks;
            new_blue  += current_color.get_blue()  + next_color.get_blue()  * ks;
        }

        current_color.set_red(MIN(new_red, 255));
        current_color.set_green(MIN(new_green, 255));
        current_color.set_blue(MIN(new_blue, 255));

        return current_color;
    }
}

void Scene::clean()
{
    s.fill_rect(0,0,s.width(),s.height(),rt::color::BLACK);
}


color Scene::ray_tracing(Ray r, int depth)
{
    Hit ht = next_hit(r);

    Object* closest_object = get_closest_object(r);

    /* If the ray doesn't hit any object, then the color of the infinity
	   is black */
    if (closest_object == nullptr)
        return color(0, 0, 0);

    vector n         = ht.get_normal().unit();
    color  obj_color = closest_object->get_color();
    rt::vector reflected_ray_direction = reflexion(ht);

    color current_color(0, 0, 0);

    double new_red   = 0;
    double new_green = 0;
    double new_blue  = 0;

    // This part handles diffuse enlightenment
    for (unsigned i = 0; i < lights.size(); ++i){
        rt::vector direction = lights.at(i)->get_hit_direction(ht).unit();

        if (!lights.at(i)->is_reachable(this, ht))
            continue;

        color light_color = lights.at(i)->get_color();

        double kd = closest_object->get_diffuse();

        double theta = std::abs((direction | n)/(direction.norm()*n.norm()));

        new_red   += obj_color.get_red()   * light_color.get_red()   * theta * kd / 255;
        new_green += obj_color.get_green() * light_color.get_green() * theta * kd / 255;
        new_blue  += obj_color.get_blue()  * light_color.get_blue()  * theta * kd / 255;

        //Specular highlights if the light is not ambient and the object is not a plan
        if (lights.at(i)->get_type() != 0 && closest_object->get_type() != 1)
        {
            double spec_ratio    = std::abs((direction.unit() | reflected_ray_direction.unit()));
            double spec_exponent = closest_object->get_specular_exponent();
            new_red   += light_color.get_red()   * pow(spec_ratio, spec_exponent);
            new_green += light_color.get_green() * pow(spec_ratio, spec_exponent);
            new_blue  += light_color.get_blue()  * pow(spec_ratio, spec_exponent);
        }
    }
    // End of diffuse enlightenment

    // The following part handles reflexion
    if (depth > 0)
    {
        // We consider the hit is slightly outside the sphere to avoid collisions
        rt::Ray reflected_ray(ht.get_point() + reflected_ray_direction*EPSILON, reflected_ray_direction);

        // Color of the ray that comes from the light and goes to the object
        color next_color = ray_tracing(reflected_ray, depth - 1);

        double ks = closest_object->get_specular();

        new_red   += current_color.get_red()   + next_color.get_red()   * ks;
        new_green += current_color.get_green() + next_color.get_green() * ks;
        new_blue  += current_color.get_blue()  + next_color.get_blue()  * ks;
    }

    current_color.set_red(MIN(new_red, 255));
    current_color.set_green(MIN(new_green, 255));
    current_color.set_blue(MIN(new_blue, 255));

    return current_color;
}


rt::vector Scene::reflexion(Hit h){
    rt::vector v = h.get_ray().get_direction();
    rt::vector n = h.get_normal();

    rt::vector d = v - 2*(v | n)*n;

    return d;
}


rt::Hit Scene::next_hit(Ray r) {
    Object* closest_obj = get_closest_object(r);

    if (closest_obj != nullptr)
    {
        Hit h = closest_obj->intersect(r);
        return h;
    }
    else
    {
        Hit h = Hit(r, rt::vector(0.0,0.0,0.0), rt::vector(0.0,0.0,0.0), color(0,0,0));
        return h;
    }
}


void Scene::add_object(Object* object)
{
    objects.push_back(object);
}


void Scene::add_light(Light* light)
{
    lights.push_back(light);
}


Object* Scene::get_closest_object(Ray r)
{
    std::numeric_limits<double> real;

    int    closest_object = -1;
    double closest_dist   = real.infinity();

    for(unsigned int k = 0; k < objects.size(); ++k)
    {
        double dist_of_k = objects.at(k)->send(r);
        //If we are inside an object
        if (closest_dist < 0)
            return nullptr;

        if (dist_of_k < closest_dist)
        {
            closest_object = k;
            closest_dist   = dist_of_k;
        }
    }

    //If no object has been found
    if (closest_object == -1)
        return nullptr;

    return objects.at(closest_object);
}
